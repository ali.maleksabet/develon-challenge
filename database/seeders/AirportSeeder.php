<?php

namespace Database\Seeders;

use App\Models\Airport;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonAirports = Storage::get('data/airports.json');
        $airports = json_decode($jsonAirports, true);

        foreach ($airports as $airport) {
            // to filter some abnormal data
            if (strlen($airport['code']) > 4) {
                continue;
            }
            Airport::firstOrCreate(
                ['code' => $airport['code']],
                [
                    'name' => $airport['name'],
                    'city_name' => $airport['city'],
                    'country_name' => $airport['country'],
                    'geo_loc' => new Point($airport['_geoloc']['lat'], $airport['_geoloc']['lng']),
                ]
            );
        }
    }
}
