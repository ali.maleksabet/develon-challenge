<?php

namespace Database\Seeders;

use App\Models\Airport;
use App\Models\AirportRoute;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use JsonMachine\JsonMachine;
use Location\Coordinate;
use Location\Distance\Vincenty;

class AirportRoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $airportRoutes = JsonMachine::fromString(Storage::get('data/airportRoutes.json'));
        $calculator = new Vincenty();

        foreach ($airportRoutes as $airportRoute) {
            $source = $airportRoute['s'];
            $destination = $airportRoute['d'];
            try {
                $sourceModel = Airport::where('code', $source)->firstOrFail();
                $destinationModel = Airport::where('code', $destination)->firstOrFail();
            } catch (ModelNotFoundException $exception) {
                continue;
            }
            $sourceCoordinate = new Coordinate($sourceModel->geo_loc->getLat(), $sourceModel->geo_loc->getLng());
            $destinationCoordinate = new Coordinate($destinationModel->geo_loc->getLat(), $destinationModel->geo_loc->getLng());

            $distanceInMeter = $calculator->getDistance($sourceCoordinate, $destinationCoordinate);
            $distanceInKilometer = $distanceInMeter / 1000;
            AirportRoute::firstOrCreate(
                [
                    'source_airport_id' => $sourceModel->id,
                    'destination_airport_id' => $destinationModel->id
                ],
                [
                    'source_airport_code' => $sourceModel->code,
                    'destination_airport_code' => $destinationModel->code,
                    'distance_in_kilometer' => $distanceInKilometer
                ]
            );
        }
    }
}
