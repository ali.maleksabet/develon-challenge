<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment("airport name");
            $table->string('city_name')->comment("airport city name");
            $table->string('country_name')->comment("airport country name");
            $table->string("code",4)->index()->unique()->comment("airport unique identifying code");
            $table->point('geo_loc')->comment("airport geometric location");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airports');
    }
}
