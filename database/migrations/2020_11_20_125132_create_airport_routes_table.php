<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport_routes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("source_airport_id")->comment("source airport id foreign key");
            $table->unsignedBigInteger("destination_airport_id")->comment("destination airport id foreign key");
            $table->string("source_airport_code", 4)->index()->comment("source airport unique code");
            $table->string("destination_airport_code", 4)->index()->comment("source airport unique code");
            $table->unsignedDouble("distance_in_kilometer")->comment("distance of two airports in kilometer");
            $table->timestamps();

            $table->foreign('source_airport_id')->references('id')->on('airports');
            $table->foreign('destination_airport_id')->references('id')->on('airports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airport_routes');
    }
}
