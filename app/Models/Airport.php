<?php

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory;
    use SpatialTrait;
    protected $spatialFields = [
        'geo_loc'
    ];
    protected $fillable = [
        'name',
        'city_name',
        'country_name',
        'code',
        'geo_loc'
    ];
}
